import Recorder from '@/utils/Recorder';
import { message } from 'antd';
import { sha256 } from 'js-sha256';
import React, { useEffect, useRef, useState } from 'react';
import { useModel } from 'umi';
import cstyle from '../common.less';
import style from './test.less';

const langArr = ['cn', 'sichuanese', 'cantonese', 'en'];
let interalHandler: any;
let recorder: any;
let ws: any;
let buffers: any[] = [];
let stoped = false;
function toTwo(val: number) {
  return val > 10 ? val : '0' + val;
}
function toMin(val: number = 0) {
  return toTwo(Math.floor(val / 60)) + ':' + toTwo(val % 60);
}
let times = 0;
export default () => {
  const [current, setCurrent] = useState(0);
  const [recording, setRecording] = useState(false);
  // const [time, setTime] = useState(0);
  const [result, setResult] = useState<Array<any>>([]);
  const [changing, setChangeing] = useState<string>('');
  const { appkey, secret, path, count } = useModel('useSettingsModel');
  const elRef: any = useRef();
  const elFixed: any = useRef();
  const elChanging: any = useRef();
  const elTime: any = useRef();

  function startRecording() {
    setRecording(true);
    buffers = [];
    setResult([]);
    setChangeing('');
    elFixed.current = '';
    let ctime = 0;
    if (elTime.current) {
      elTime.current.innerHTML = '0:00';
    }
    interalHandler = setInterval(() => {
      ctime = ctime + 1;
      if (elTime.current) {
        elTime.current.innerHTML = toMin(ctime);
      }
    }, 1000);
    doRecording();
  }
  function endRecording() {
    clearInterval(interalHandler);
    setRecording(false);
    // setTime(0);
    // ctime = 0;
    stopRecording();
  }

  function doRecording() {
    recorder = new Recorder(onaudioprocess);
    createWS();
    recorder.ready().then(
      () => {
        recorder.start();
      },
      () => {
        message.warn('录音启动失败！');
        if (ws) ws.close();
      },
    );
  }

  function createWS() {
    // ws仅用于demo，正式使用请使用wss
    const tm: number = +new Date();
    const sign = sha256(`${appkey}${tm}${secret}`).toUpperCase();
    let sid: any;
    ws = new WebSocket(`${path}?appkey=${appkey}&time=${tm}&sign=${sign}`);
    ws.onopen = () => {
      ws.send(
        JSON.stringify({
          type: 'start',
          sha: '256',
          data: {
            lang: langArr[current]
          },
        }),
      );

      // ws.close();
    };
    // let fixed: string[] = [...result];
    ws.onmessage = (evt: any) => {
      const res = JSON.parse(evt.data);
      if (res.code == 0 && res.text) {
        if (res.type === 'fixed') {
          // elFixed.current += res.text;
          result.push(res.text);
          if (result.length > count) {
            result.shift();
          }
          setResult([...result]);
          setChangeing('')
        } else {
          setChangeing(res.text);
        }
        sid = res.sid;
      }
      if(res.end) { // res.end === true 时才识别结束，断开websocket
        console.log(new Date().valueOf(), '识别结束', res);
        ws.close()
      }
    };
    ws.onerror = function(e: any) {
      recorder.stop();
      console.log(new Date().valueOf(), 'ws error', sid, e);
      alert('websocket error');
      ws = null;
    };

    ws.onclose = (e: any) => {

      console.log(new Date().valueOf(), 'ws close', sid, e);
      // alert('websocket 断开');
      ws = null;
      if (times < 1) {
        createWS();
        times++;
      } else {
        if (times > 10) {
          recorder.stop();
          alert('websocket 断开');
          return;
        }
        setTimeout(createWS, 100);
        times++;
      }
    };
  }

  function onaudioprocess(buffer: any) {
    console.log(new Date().valueOf(), '-onaudioprocess:', ws?.readyState);

    if (ws && ws.readyState === 1) {
      console.log('ws send buffer:', buffer.byteLength);
      ws.send(buffer);
    }
  }
  function stopRecording() {
    if (!recorder) return;
    recorder.stop();
    if (ws && ws.readyState === 1) {
      ws.send(
        JSON.stringify({
          type: 'end',
        }),
      );
    }
  }
  useEffect(() => {
    if (elRef.current) {
      elRef.current.scrollTop = elRef.current.scrollHeight;
    }
  }, [result, changing]);
  console.log('refreash');
  return (
    <div
      style={{
        backgroundColor: '#f7f9fb',
        paddingBottom: 50,
      }}
      className={cstyle.bigBlock}
      id="test"
    >
      <div className={cstyle.title}>实时语音转写</div>
      {/* <div>
        <Button
          type="primary"
          onClick={() => {
            let arr = new Array(800);
            arr.fill(
              `中共中央对外联络部发言人胡兆明5日宣布，中国共产党与世界政党领导人峰会将于7月6日以视频连线方式举行，中共中央总书记、国家主席习近平将在北京出席会议并发表主旨讲话。此次会议的主题为“为人民谋幸福：政党的责任”，160多个国家的500多位政党和政治组织等领导人以及逾万名政党代表等将出席会议。`,
            );
            elFixed.current.innerHTML = arr.join('');
          }}
        >
          测试内存占用
        </Button>
      </div> */}
      <div className={style.boxContainer}>
        <div className={style.box}>
          <div className={cstyle.subTitle}>语音录入</div>
          <div className={style.borderBox}>
            <ul className={style.tabs}>
              <li
                className={`${style.tab} ${current === 0 ? style.active : ''}`}
                onClick={() => !recording && setCurrent(0)}
              >
                普通话
              </li>
              {/* <li
                className={`${style.tab} ${current === 1 ? style.active : ''}`}
                onClick={() => setCurrent(1)}
              >
                四川话
              </li>
              <li
                className={`${style.tab} ${current === 2 ? style.active : ''}`}
                onClick={() => setCurrent(2)}
              >
                粤语
              </li> */}
              <li
                className={`${style.tab} ${current === 3 ? style.active : ''}`}
                onClick={() => !recording && setCurrent(3)}
              >
                英语
              </li>
            </ul>
            <div className={style.micbox}>
              <div
                onClick={() => {
                  if (!recording) {
                    // setResult([]);
                    stoped = false;
                    times = 0;
                    startRecording();
                  } else {
                    stoped = true;
                    endRecording();
                  }
                }}
                className={recording ? style.imgRecording : style.img}
              >
                {recording && (
                  <div className={style.imgtext}>
                    <img src={require('@/assets/icon_pause.png')} height="33" />
                    <div>
                      <span style={{ color: '#FFB700' }} ref={elTime}></span>
                    </div>
                  </div>
                )}
              </div>
              {!recording && (
                <div className={style.desc}>
                  点击“麦克风”开始录音，请对我说想说的话，我可以识别出你说的内容。请允许浏览器获取麦克风权限。
                </div>
              )}
            </div>
          </div>
        </div>
        <div className={style.box}>
          <div className={cstyle.subTitle}>识别结果</div>
          <div className={style.borderBox + ' ' + style.result} ref={elRef}>
            <span ref={elFixed}>{result.join('')}</span>
            <span ref={elChanging}>{changing}</span>
          </div>
        </div>
      </div>
      {/* <div style={{ margin: 30, textAlign: 'right' }}>
        <Button
          type="primary"
          onClick={() => {
            if (elFixed.current?.length > 0) {
              var ele = document.createElement('a');
              ele.download = 'result.txt';
              ele.style.display = 'none';
              var blob = new Blob([elFixed.current]);
              ele.href = URL.createObjectURL(blob);
              document.body.appendChild(ele);

              ele.click();
              //移除元素
              document.body.removeChild(ele);
            } else {
              message.error('没有识别结果');
            }
          }}
        >
          下载识别结果
        </Button>
      </div> */}
    </div>
  );
};
