import React, { FC, useState, useEffect } from 'react';
import { Layout } from 'antd';
import styles from './index.less';
import Asr from './asrRealTime/Test';
import BaseSetting from './BaseSetting';

interface IndexProps {
  children?: React.ReactNode;
}
import UserHeader from '@/components/UserHeader';
const { Content } = Layout;
const Index: FC<IndexProps> = ({ children }) => {
  return (
    <Layout className={styles.page}>
      <Content className={styles.content}>
        <BaseSetting />
        <Asr />
      </Content>
    </Layout>
  );
};
export default Index;
