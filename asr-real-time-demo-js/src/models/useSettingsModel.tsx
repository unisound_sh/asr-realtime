import { useState } from 'react';
import { AiCode, Config } from '../config'
export default function useDevicesModel() {
  const [appkey, setAppkey] = useState(
    Config[AiCode.Real].appKey
  );
  const [secret, setSecret] = useState(Config[AiCode.Real].secret);
  const [path, setPath] = useState(Config[AiCode.Real].path);
  const [count, setCount] = useState(10);
  return {
    appkey,
    setAppkey,
    secret,
    setSecret,
    path,
    setPath,
    count,
    setCount,
  };
}
